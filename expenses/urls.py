from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_home(request):
    return redirect("/receipts/")


urlpatterns = [
    path("", redirect_to_home),
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("", include("accounts.urls")),
]
